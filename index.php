<?php
    require_once('connect.php');
     require_once('heading.php'); 
?>
<table>
    <thead>
        <tr>
            <th>N</th>
            <th>Name</th>
            <th>Description</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th style="width: 60px;">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php 
		$total = mysqli_query($conn, "SELECT * FROM task");
        $page=1;//khởi tạo trang ban đầu
		$limit=10;//số bản ghi trên 1 trang (10 bản ghi trên 1 trang)
        $total_record = mysqli_num_rows($total);
        $total_page=ceil($total_record/$limit);//tính tổng số trang sẽ chia
        if(isset($_GET["page"]))
			$page=$_GET["page"];//nếu biến $_GET["page"] tồn tại thì trang hiện tại là trang $_GET["page"]
		if($page<1) $page=1; //nếu trang hiện tại nhỏ hơn 1 thì gán bằng 1
        if($page>$total_page) $page=$total_page;
        $start=($page-1)*$limit;
        $task = mysqli_query($conn,"select * from task ORDER BY updated_at DESC limit $start,$limit");
		while ($row = mysqli_fetch_array($task)) { ?>
        <tr>
            <td> <?php echo $row['id']; ?> </td>
            <td class="task"> <?php echo $row['name']; ?> </td>
            <td><?php echo $row['decription']; ?></td>
            <td><?php echo $row['created_at']; ?></td>
            <td><?php echo $row['updated_at']; ?></td>
            <td class="delete">
                <a href="deleteTask.php?id=<?php echo $row['id']; ?>">xoá </a>
                <a href="editTask.php?id=<?php echo $row['id']; ?>">Sửa </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="headinga">
    <button type="button" name="addTask" id="add_btn" class="add_btn"><a href="addtask.php">Add Task</a> </button>
</div>
<div class="headinga">
    <ul class="pagination">
        <?php for($i=1;$i<=$total_page;$i++){ ?>
        <li <?php if($page == $i) echo "class='active'"; ?>><a
                href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php } ?>
    </ul>
</div>
</body>
</html>